Exercise:

Explain the following commands with one sentence:

    $ git diff						Zeigt die Unterschiede zwischen dem Arbeitsverzeichnis und der Stage
    $ git diff --staged					Zeigt die Unterschiede zwischen der Stage und dem letzten comitteten Zustand
    $ git diff test.txt					Zeigt die Unterschiede der Datei test.txt zwischen Arbeitsverzeichnis und der Stage
    $ git diff --theirs					Zeigt bei einem Merge-Konflikt die konflikterzeugenden Änderungen auf dem Zielbranch
    $ git diff --ours					Zeigt bei einem Merge-Konflikt die konflikterzeugenden Änderungen auf dem aktuellen Branch
    $ git log						Zeigt die Commit-Historie
    $ git log --oneline					Zeigt die Commit-Historie: ein Commit - eine Zeile
    $ git log --oneline --all				Zeigt die Commit-Historie: ein Commit - eine Zeile, inklusive der 'toten' commits
    $ git log --oneline --all --graph			Zeigt die Commit-Historie in einer Baumstruktur: ein Commit - eine Zeile, inklusive der 'toten' commits
    $ git log --follow -p -- filename			Zeigt die Commit-Historie der Datei filename in Form von Patches, Umbenennungen werden berücksichtigt
    $ git log -S'static void Main'			Zeigt alle Commits, die 'static void Main' im Commit-Kommentar enthalten
    $ git log --pretty=format:"%h - %an, %ar : %s"	Zeigt die Commit-Historie im Format: Hash - Autor, relative Zeitangabe des Commits : Commit-Kommentar 

Example:

    $ git status         Shows the current state of the repository

First, add your answers in this file and commit/push.
Later, integrate your answers into the README.md.