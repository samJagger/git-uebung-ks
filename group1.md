Exercise:

Explain the following commands with one sentence:

    $ git init                    Create a new local git repository
    $ git clone https://...       Clone a Remote Repository
    $ git add test.txt            The file test.txt file is added to the stage
    $ git reset HEAD test.txt     Reverting to the state of the HEAD
    $ git add .                   All edited files are added to the stage except deleted files
    $ git add -u                  Only modified files where staged
    $ git add -A                  Everything will be staged
    $ git diff --staged           Showing the changes between the staged files and the current HEAD
    $ git commit                  Commit staged files. A commit message has to be specified afterwards
    $ git commit -a               Only modified files where staged and commited
    $ git commit -m "My message"  Commit staged files using the given commit message

Example:

    $ git status         Shows the current state of the repository

First, add your answers in this file and commit/push.
Later, integrate your answers into the README.md.